package jupiter.autotools.report.mutithread;

import java.util.concurrent.CountDownLatch;

/**
 * Created by dextry on 2017/6/9.
 */
public class MutiThreadReadFile {
    /**
     * @param args
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        final int thNum = 4;
        final String filePath3 = "C:\\gtja\\2_2_test_21w - 副本.txt"; //266M
        final String filePath2 = "C:\\gtja\\2_2_test_21w - 副本(2).txt"; //186M
        final String filePath = "C:\\gtja\\2_2_test_21w - 副本(3).txt"; //39KB
        final String filePath4 = "C:\\gtja\\2_2_test_21w - 副本(4).txt"; //1KB

        CountDownLatch doneSignal = new CountDownLatch(thNum);
        ReadFileThread2 r1 = new ReadFileThread2(doneSignal,filePath);
        ReadFileThread2 r2 = new ReadFileThread2(doneSignal,filePath2);
        ReadFileThread2 r3 = new ReadFileThread2(doneSignal,filePath3);
        ReadFileThread2 r4 = new ReadFileThread2(doneSignal,filePath4);
        r1.start();
        r2.start();
        r3.start();
        r4.start();
        try {
            doneSignal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        System.out.println("===============================");
        System.out.println("The totally executed time: "+(endTime-startTime));
    }
}
