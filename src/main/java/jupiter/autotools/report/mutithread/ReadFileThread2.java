package jupiter.autotools.report.mutithread;

import java.io.*;
import java.util.concurrent.CountDownLatch;

/**
 * Created by dextry on 2017/6/9.
 */
class ReadFileThread2 extends Thread{
    private RandomAccessFile raf;
    private CountDownLatch doneSignal;
    private final int bufLen = 256;
    private String path;

    public ReadFileThread2(CountDownLatch doneSignal,String path){
        this.doneSignal = doneSignal;
        this.path = path;
    }


    @Override
    public void run() {
        long start = System.currentTimeMillis();
        try {
            raf = new RandomAccessFile(path,"rw");
            raf.seek(0);
            long contentLen = new File(path).length();
            long times = contentLen / bufLen +1;
            byte []buff = new byte[bufLen];
            int hasRead = 0;
            String result = null;
            for(int i=0;i<times;i++){
                hasRead = raf.read(buff);
                if(hasRead < 0){
                    break;
                }
                result = new String(buff,"gb2312");
            }
            doneSignal.countDown();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println(getName() + " " + path +" total Time: " + (end - start));
    }
}