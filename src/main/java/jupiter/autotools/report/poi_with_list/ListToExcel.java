package jupiter.autotools.report.poi_with_list;

/**
 * Created by dextry on 2017/6/8.
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jupiter.autotools.report.MyJob;
import jupiter.autotools.report.util.LoggerUtil;
import org.apache.poi.hssf.record.InterfaceEndRecord;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;

public class ListToExcel {
    private static LoggerUtil logger = LoggerUtil.getInstance(ListToExcel.class);
    /**
     *
     * @param is
     * @param sheetName
     * @return
     */
    public List<Map<String, String>> readExcel(InputStream is, String sheetName) {
        System.out.println("11111111113333333333");
        List<Map<String, String>> list = new ArrayList();
        List<String> titleList = new ArrayList();
        HSSFWorkbook workbook = null;
        // 工作表
        HSSFSheet sheet = null;
        try {
            workbook = new HSSFWorkbook(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sheetName == null || sheetName.equals("")) {
            sheet = workbook.getSheet("Sheet1");
        } else {
            sheet = workbook.getSheet(sheetName);
        }
        System.out.println("1111111111" + sheet.getLastRowNum());
        System.out.println("222222222" + sheet.getPhysicalNumberOfRows());
        String firstcell = (sheet.getRow(0).getCell(0).getStringCellValue());
        System.out.println(firstcell);
        int totalRows = sheet.getPhysicalNumberOfRows();
        int firstNumber = sheet.getRow(0).getPhysicalNumberOfCells();
        for (int n = 0; n < firstNumber; n++) {
            String firstCellValue = sheet.getRow(0).getCell(n)
                    .getStringCellValue();
            titleList.add(firstCellValue);
        }
        System.out.println("第二行：");
        for (int i = 1; i < totalRows; i++) {
            Map map = new HashMap();
            int cells = sheet.getRow(i).getPhysicalNumberOfCells();
            System.out.println("the" + i + "row has " + cells + "cells");

            HSSFRow row = sheet.getRow(i);
            String str = "";
            for (int j = 0; j < titleList.size(); j++) {
                System.out.println("j==========" + j);
                HSSFCell cell = row.getCell((short) j);
                if (cell != null) {
                    try {
                        switch (cell.getCellType()) {
                            case HSSFCell.CELL_TYPE_STRING:
                                str = cell.getStringCellValue();
                                break;
                            case HSSFCell.CELL_TYPE_NUMERIC:
                                // 判断当前的cell是否为Date
                                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                    // 如果是Date类型则，取得该Cell的Date值
                                    Date date = cell.getDateCellValue();
                                    // 把Date转换成本地格式的字符串
                                    str = cell.getDateCellValue().toLocaleString();
                                }
                                // 如果是纯数字
                                else {
                                    // 取得当前Cell的数值
                                    int num = new Integer((int) cell
                                            .getNumericCellValue());
                                    str = String.valueOf(num);
                                }       break;
                            case HSSFCell.CELL_TYPE_FORMULA:
                                str = String.valueOf(cell.getNumericCellValue());
                                break;
                            case HSSFCell.CELL_TYPE_BLANK:
                                str = "";
                                break;
                            case HSSFCell.CELL_TYPE_BOOLEAN:
                                break;
                            default:
                                str = "";
                                break;
                        }
                    } catch (Exception e) {
                        continue;
                    }
                    map.put(titleList.get(j), str);
                    str = "";
                } else {
                    cell = row.createCell((short) j);
                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                    cell.setCellValue("");
                    map.put(titleList.get(j), "");
                }
            }
            list.add(map);
            System.out.println("mapmmmmmmmm="+map);
        }
        if (is != null) {
            try {
                is.close();
                is = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //System.out.println(titleList);
        //System.out.println(list);
        return list;
    }


    public  static  void createNewExcel(List<List<Object>> arrayLists, ArrayList head,
                                        String outPutPath, String filename)  {
        logger.info("createNewExcel()", "begin to export to excel");
        try {
            FileOutputStream output = new FileOutputStream(outPutPath + File.separator + filename + ".xls");
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFCellStyle style = workbook.createCellStyle();
            // 在excel中新建一个工作表，名字为jsp
            HSSFSheet sheet = workbook.createSheet("data");

            /**
             * 设置其它数据 设置风格
             */
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN); // 设置单无格的边框为粗体
            style.setBottomBorderColor(HSSFColor.BLACK.index); // 设置单元格的边框颜色．
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setLeftBorderColor(HSSFColor.BLACK.index);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(HSSFColor.BLACK.index);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setTopBorderColor(HSSFColor.BLACK.index);
            style.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            //样式二（列名样式）
            // 生成一个样式

            HSSFCellStyle style2 = workbook.createCellStyle();

            style2.setFillForegroundColor(HSSFColor.SKY_BLUE.index);

            style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

            style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);

            style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            style2.setBorderRight(HSSFCellStyle.BORDER_THIN);

            style2.setBorderTop(HSSFCellStyle.BORDER_THIN);

            style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

            // 生成一个字体

            HSSFFont font = workbook.createFont();

            font.setColor(HSSFColor.VIOLET.index);

            font.setFontHeightInPoints((short) 12);

            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            // 把字体应用到当前的样式

            style2.setFont(font);
            // style.setWrapText(true);//文本区域随内容多少自动调整

            int cellNumber = arrayLists.get(0).size();
            int rowNumber = arrayLists.size();
            // 创建第一行
            HSSFRow row1 = sheet.createRow(0);

            //列名项
            for (int i = 0; i < cellNumber; i++) {
                HSSFCell cell = row1.createCell(i);
                cell.setCellValue((String) head.get(i));
                cell.setCellStyle(style2);
            }
            for (int rown = 0; rown < rowNumber; rown++) {
                HSSFRow row = sheet.createRow(rown + 1);
                for (int celln = 0; celln < cellNumber; celln++) {
                    List<Object> rowlist = arrayLists.get(rown);
                    HSSFCell cell = row.createCell(celln);
                    cell.setCellValue((String) rowlist.get(celln));
                    cell.setCellStyle(style);
                }
            }
            output.flush();
            workbook.write(output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("createNewExcel()", e);
        }
    }

    public static void main(String[] args) {

            List<List<Object>> arrayLists = new ArrayList<List<Object>>();
            for (int i=0;i<4;i++){
                ArrayList<Object> fir = new ArrayList<Object>();
                fir.add("ming"+i);
                fir.add("huang"+i);
                fir.add("green"+i);
                fir.add("red"+i);
                arrayLists.add(fir);
            }
            ArrayList<Object> head = new ArrayList<Object>();
            head.add("ming");
            head.add("huang");
            head.add("green");
            head.add("red");


            createNewExcel(arrayLists,head,"C:\\opt", "myjupiter");
        }
    }

