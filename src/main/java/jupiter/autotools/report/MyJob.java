package jupiter.autotools.report;


import jupiter.autotools.report.poi_with_list.ListToExcel;
import jupiter.autotools.report.util.JdbcUtil;
import jupiter.autotools.report.util.LoggerUtil;
import jupiter.autotools.report.util.sqlserverUtil;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by dextry on 2017/5/25.
 */
public class MyJob implements Job {
    private static String downloadPath;
    private static LoggerUtil logger = LoggerUtil.getInstance(MyJob.class);
    private static int count;

    static {
        Properties properties = new Properties();
        InputStream inputStream = null;

        try {
            inputStream = ReadConfig.class.getResourceAsStream("/config.properties");
            properties.load(inputStream);

            downloadPath = properties.getProperty("downloadPath");

        } catch (IOException e) {
            System.out.println(e);
            logger.error("myjob.static()", e);
        }  finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("myjob.static()", e);
            }
        }
    }
  
    public void execute(JobExecutionContext jobContext) throws JobExecutionException {


        System.out.println("--------------------------------------------------------------------");

        System.out.println("MyJob start: " + jobContext.getFireTime());
        logger.info( "MyJob.execute()","MyJob start: " + jobContext.getFireTime());

        JobDetail jobDetail = jobContext.getJobDetail();
        System.out.println("MyJob end: " + jobContext.getJobRunTime() + ", key: " + jobDetail.getKey());
        System.out.println("MyJob next scheduled time: " + jobContext.getNextFireTime());
        String reportUrl = (String) jobDetail.getJobDataMap().get("reportUrl");
        String user = (String) jobDetail.getJobDataMap().get("user");
        String password = (String) jobDetail.getJobDataMap().get("password");
        String sql = (String) jobDetail.getJobDataMap().get("sql");
        String email = (String) jobDetail.getJobDataMap().get("email");
        String filenameSuffix = (String) jobDetail.getJobDataMap().get("filenameSuffix");
        Connection connection = null;
        try {
        if(reportUrl.startsWith("jdbc:sqlserver")){
             connection = sqlserverUtil.getConnection(reportUrl, user, password);
        }else {
             connection = JdbcUtil.getConnection(reportUrl, user, password);
        }

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);


            int count = 0;
            List<List<Object>> arrayLists = new ArrayList<List<Object>>();
            ArrayList<Object> head = new ArrayList<Object>();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                // resultSet数据下标从1开始
                head.add(metaData.getColumnName(i + 1));
            }
            while (resultSet.next()) {
                ArrayList<Object> content = new ArrayList<Object>();
                int j = 0;
                String column;
                while (j < metaData.getColumnCount()) {
                    column = resultSet.getString(j + 1);
                    content.add(column);
                    j++;
                }
                arrayLists.add(content);
                count++;
            }
            String filename = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()) + "_" + filenameSuffix;
            //CSVUtils.createCSVFile(head, arrayLists, downloadPath, filename);
            ListToExcel.createNewExcel(arrayLists,head,downloadPath,filename);
            System.out.println(Calendar.getInstance().getTime() + " - " + "write " + count);

            String[] parameters = new String[]{
                    downloadPath, filename, email
            };
            JavaMailWithAttachment.main(parameters);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("myJob.execute()",e);
        }


        System.out.println("--------------------------------------------------------------------");
  }
}

